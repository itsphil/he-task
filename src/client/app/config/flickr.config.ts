export const FlickrConfig = {
    ENDPOINTS: {
        GET_RECENT: 'flickr.photos.getRecent',
        SEARCH: 'flickr.photos.search'
    }
};