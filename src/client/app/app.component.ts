import { Component } from '@angular/core';
import './operators';

@Component({
	moduleId: module.id,
	selector: 'he-app',
	templateUrl: 'app.component.html',
	styleUrls: ['app.component.css'],
})
export class AppComponent {

    constructor() { }

}
