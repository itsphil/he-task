import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'he-image',
    templateUrl: 'image.component.html',
    styleUrls: ['image.component.css'],
})
export class ImageComponent {

    private _image: any;
    @Input() private set image(img: any) {
        this._image = img;

        this.processImage();
    }
    private get image(): any {
        return this._image;
    }

    constructor() {}

    private processImage(): void {
        this._image.tagsArray = this.getImageTags();
    }

    private getImageTags(): string[] {
        return (this._image.tags.length > 0) ? this._image.tags.split(' ') : null
    }

}