import { Component } from '@angular/core';

import { ApiRequest_Search, ApiRequestExtras } from '../../models/request';
import { FlickrService } from '../../services/flickr.service';

@Component({
    moduleId: module.id,
    selector: 'he-image-feed',
    templateUrl: 'imageFeed.component.html',
    styleUrls: ['imageFeed.component.css'],
})
export class ImageFeedComponent {

    private tag_search: string = 'airport,hotels';
    private images: any[];

    constructor(private flickr: FlickrService) {
        this.doSearch();
    }

    private doSearch(): void {
        const request: ApiRequest_Search = new ApiRequest_Search();
        request.content_type = 4;
        request.safe_search = 1;
        request.tags = `holiday,${this.tag_search}`;
        request.tag_mode = 'all';
        request.extras = {
            description: true,
            owner_name: true,
            tags: true,
            url_m: true
        }

        this.flickr.search(request).subscribe((result: any) => {
            this.images = result.photos.photo;
        });
    }
    
}