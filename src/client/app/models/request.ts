export class ApiRequest_Base {
    content_type?: number;
    extras?: ApiRequestExtras;
    per_page?: number;
    page?: number;
    safe_search?: number;
}

export class ApiRequest_Search extends ApiRequest_Base {
    tags: string;
    tag_mode: string;
}

export class ApiRequestExtras {
     description?: boolean;
     license?: boolean;
     date_upload?: boolean;
     date_taken?: boolean;
     owner_name?: boolean;
     icon_server?: boolean;
     original_format?: boolean;
     last_update?: boolean;
     geo?: boolean;
     tags?: boolean;
     machine_tags?: boolean;
     o_dims?: boolean;
     views?: boolean;
     media?: boolean;
     path_alias?: boolean;
     url_sq?: boolean;
     url_t?: boolean;
     url_s?: boolean;
     url_q?: boolean;
     url_m?: boolean;
     url_n?: boolean;
     url_z?: boolean;
     url_c?: boolean;
     url_l?: boolean;
     url_o?: boolean;
}