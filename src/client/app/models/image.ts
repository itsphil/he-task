export class Image {
    title: string;
    link: string;
    media: { [key: string]: string };
    date_taken: string;
    description: string;
    published: string;
    author: string;
    author_id: string;
    tags: string;
}