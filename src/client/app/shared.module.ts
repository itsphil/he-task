import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ImageComponent } from './components/image/image.component';
import { ImageFeedComponent } from './components/imageFeed/imageFeed.component';

import { FlickrService } from './services/flickr.service';

@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [ImageComponent, ImageFeedComponent],
    providers: [FlickrService],
    exports: [ImageComponent, ImageFeedComponent]
})
export class SharedModule { }
