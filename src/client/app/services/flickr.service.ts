import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import * as _ from 'underscore';
import { Observable } from 'rxjs/Observable';
import { Config } from '../config/env.config';
import { FlickrConfig } from '../config/flickr.config';
import { ApiRequest_Base, ApiRequest_Search, ApiRequestExtras } from '../models/request';

@Injectable()
export class FlickrService {
    
    constructor(private http: Http) {}

    private call(method: string, request: ApiRequest_Base): Observable<any> {
        return this.http.get(this.generateURL(method, request))
            .map(result => this.extractResult(result))
            .catch(err => this.handleError(err));
    }

    private extractResult(result: Response): any {
        const data = result.json() || {};
        console.log(data);

        if(data.stat === 'fail') return this.handleError(data);

        return data;
    }

    private handleError(err: any | Response): Observable<any> {
        return err;
    }

    private generateURL(method: string, request: ApiRequest_Base | ApiRequest_Search): string {
        const tags: string = this.processRequestTags((request as ApiRequest_Search).tags);
        const tag_mode: string = this.processRequestTagMode((request as ApiRequest_Search).tag_mode);
        const extras: string = this.processRequestExtras(request.extras);
        const perPage: string = this.processRequestPerPage(request.per_page);
        const page: string = this.processRequestPage(request.page);

        return `${Config.API}?api_key=${Config.API_KEY}&format=${Config.API_FORMAT}&method=${method}&${tags}&${tag_mode}&${extras}&${perPage}&${page}&nojsoncallback=?`;
    }

    public getRecent(request: ApiRequest_Base): Observable<any> {
        return this.call(FlickrConfig.ENDPOINTS.GET_RECENT, request);
    }

    public search(request: ApiRequest_Search): Observable<any> {
        return this.call(FlickrConfig.ENDPOINTS.SEARCH, request);
    }

    private processRequestExtras(extras: ApiRequestExtras): string {
        if(!extras) return '';

        let extrasStr: string = '';

        _.forEach(extras, (extra: any, index: any) => {
            if(extra === true) extrasStr += `,${index}`;
        });

        if(extrasStr.length > 0) extrasStr = extrasStr.slice(1, extrasStr.length);

        return (extrasStr.length > 0) ? `extras=${extrasStr}` : '';
    }

    private processRequestPerPage(perPage: number): string {
        if(!perPage) return '';

        return `per_page=${perPage}`;
    }

    private processRequestPage(page: number): string {
        if(!page) return '';

        return `page=${page}`;
    }

    private processRequestTags(tags: string): string {
        if(!tags) return '';

        return `tags=${tags}`;
    }

    private processRequestTagMode(tag_mode: string): string {
        if(!tag_mode) return '';

        return `tag_mode=${tag_mode}`;
    }

}