## Holiday Extras Task (Flickr API) - Phil Zeelte

My attempt at the [Flickr API](https://github.com/holidayextras/culture/blob/master/recruitment/developer-flickr-task.md) task, set by Holiday Extras.

Production version can be viewed at [hx-task.itsphil.com](http://hx-task.itsphil.com)

### Usage
- git clone https://itsphil@bitbucket.org/itsphil/he-task.git
- npm install
- npm start