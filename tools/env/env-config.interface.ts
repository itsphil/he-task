export interface EnvConfig {
  API?: string;
  API_KEY?: string;
  API_FORMAT?: string;
  ENV?: string;
}
