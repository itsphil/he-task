import { EnvConfig } from './env-config.interface';

const BaseConfig: EnvConfig = {
  API: 'https://api.flickr.com/services/rest',
  API_KEY: 'dffb603eeec7b4a932c49303d56ca722',
  API_FORMAT: 'json'
};

export = BaseConfig;

